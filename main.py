#Program to create the website similar to bookmyshow

#import the necessary packages
# import email
# from email import message
# from sqlite3 import Cursor
from flask import Flask, render_template, request, redirect, url_for, session
from flask_mysqldb import MySQL
import MySQLdb.cursors
from flask_mail import Message, Mail
import re


app = Flask(__name__)

app.config["MAIL_SERVER"] = "smtp.gmail.com"
app.config["MAIL_PORT"] = 465
app.config["MAIL_USE_SSL"] = True
app.config["MAIL_USERNAME"] = 'sriramsriramsriram19@gmail.com'
app.config["MAIL_PASSWORD"] = 'osqdormwfcloiicj'
app.config["MAIL_USE_TLS"] = False

mail=Mail(app)

# app = Flask(__name__)

# Change this to your secret key (can be anything, it's for extra protection)
app.secret_key = 'your secret key'

# Enter your database connection details below
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'Navak@1925'
app.config['MYSQL_DB'] = 'pythonlogin2'

# Intialize MySQL
mysql = MySQL(app)


@app.route('/pythonlogin/', methods=['GET', 'POST'])
def login():
	# Output message if something goes wrong...
	msg = ''
	# Check if "username" and "password" POST requests exist (user submitted form)
	if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
		# Create variables for easy access
		username = request.form['username']
		password = request.form['password']
		# Check if account exists using MySQL
		cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
		cursor.execute('SELECT * FROM accounts WHERE username = %s AND password = %s', (username, password,))
		# Fetch one record and return result
		account = cursor.fetchone()
		# If account exists in accounts table in out database
		if account:
			# Create session data, we can access this data in other routes
			session['loggedin'] = True
			session['id'] = account['id']
			session['username'] = account['username']
			# Redirect to home page
			return redirect(url_for('home'))
		else:
			# Account doesnt exist or username/password incorrect
			msg = 'Incorrect username/password!'
	# Show the login form with message (if any)
	return render_template('index.html', msg=msg)


@app.route('/pythonlogin/login/<string:username>/<string:password>', methods=['GET', 'POST'])
def login1(username,password):
	# Check if account exists using MySQL
	cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
	cursor.execute('SELECT * FROM accounts WHERE username = %s AND password = %s', (username, password,))
	# Fetch one record and return result
	account = cursor.fetchone()
	# If account exists in accounts table in out database
	if account:
	# Create session data, we can access this data in other routes
		session['loggedin'] = True
		session['id'] = account['id']
		session['username'] = account['username']
		# Redirect to home page
		return redirect(url_for('home'))
	else:
		# Account doesnt exist or username/password incorrect
		msg = 'Incorrect username/password!'
	return render_template('index.html', msg=msg)


@app.route('/pythonlogin/query')
def query():
	# Check if account exists using MySQL
	username = request.args['username']
	password = request.args['password']
	cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
	cursor.execute('SELECT * FROM accounts WHERE username = %s AND password = %s', (username, password,))
	# Fetch one record and return result
	account = cursor.fetchone()
	# If account exists in accounts table in out database
	if account:
	# Create session data, we can access this data in other routes
		session['loggedin'] = True
		session['id'] = account['id']
		session['username'] = account['username']
		# Redirect to home page
		return redirect(url_for('home'))
	else:
		# Account doesnt exist or username/password incorrect
		msg = 'Incorrect username/password!'
	return render_template('index.html', msg=msg)


# http://localhost:41005/python/logout - this will be the logout page
@app.route('/pythonlogin/logout')
def logout():
	# Remove session data, this will log the user out
	session.pop('loggedin', None)
	session.pop('id', None)
	session.pop('username', None)
	# Redirect to login page
	return redirect(url_for('login'))

# http://localhost:41005/pythinlogin/register - this will be the registration page, we need to use both GET and POST requests
@app.route('/pythonlogin/register', methods=['GET', 'POST'])
def register():
	# Output message if something goes wrong...
	msg = ''
	# Check if "username", "password" and "email" POST requests exist (user submitted form)
	if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form:
		# Create variables for easy access
		username = request.form['username']
		password = request.form['password']
		email = request.form['email']
		# Check if account exists using MySQL
		cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
		cursor.execute('SELECT * FROM accounts WHERE username = %s', (username,))
		account = cursor.fetchone()
		# If account exists show error and validation checks
		if account:
			msg = 'Account already exists!'
		elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
			msg = 'Invalid email address!'
		elif not re.match(r'[A-Za-z0-9]+', username):
			msg = 'Username must contain only characters and numbers!'
		elif not username or not password or not email:
			msg = 'Please fill out the form!'
		else:
			# Account doesnt exists and the form data is valid, now insert new account into accounts table
			cursor.execute('INSERT INTO accounts VALUES (NULL, %s, %s, %s)', (username, password, email,))
			mysql.connection.commit()
			msg = 'You have successfully registered!'
	elif request.method == 'POST':
		# Form is empty... (no POST data)
		msg = 'Please fill out the form!'
	# Show registration form with message (if any)
	return render_template('register.html', msg=msg)


# http://localhost:41005/pythinlogin/home - this will be the home page, only accessible for loggedin users
@app.route('/pythonlogin/home')
def home():
	# Check if user is loggedin
	if 'loggedin' in session:
		# User is loggedin show them the home page
		return render_template('home.html', username=session['username'])
	# User is not loggedin redirect to login page
	return redirect(url_for('login'))

# http://localhost:41005/pythinlogin/profile - this will be the profile page, only accessible for loggedin users
@app.route('/pythonlogin/profile')
def profile():
	# Check if user is loggedin
	if 'loggedin' in session:
		# We need all the account info for the user so we can display it on the profile page
		# cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
		# cursor.execute('SELECT * FROM accounts WHERE id = %s', (session['id'],))
		# account = cursor.fetchone()
		cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
		cursor.execute('SELECT id,name,Email FROM accounts1 WHERE id = %s', (session['id'],))
		data = cursor.fetchall()
		# Show the profile page with account info
		return render_template('profile.html', account=data)
	# User is not loggedin redirect to login page
	return redirect(url_for('login'))

@app.route('/pythonlogin/mytickets')
def mytickets():
	# cur = con.cursor()
	cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
	cursor.execute("SELECT * FROM accounts1")
	data = cursor.fetchall()
	return render_template('template.html', data=data)
	
@app.route('/pythonlogin/about')
def about():
	# Check if user is loggedin
	if 'loggedin' in session:
		# User is loggedin show them the about page
		return render_template('about.html', username=session['username'])
	# User is not loggedin redirect to login page
	return redirect(url_for('login'))

@app.route('/pythonlogin/contactus',methods=['GET','POST'])
def contactus():
	if request.method == 'POST':
		email = request.form['email']
		subject = request.form['subject']
		msg = request.form['message']
		message = Message(subject,sender="sriramsriramsriram19@gmail.com",recipients=[email])
		message.body=msg 
		mail.send(message)
		success = "Message sent"
	return render_template('contactus.html', success=True)

@app.route('/pythonlogin/bookmyticket',methods = ['GET','POST'])
def bookmyticket():
	msg = ''
	if request.method == 'POST' and 'name' in request.form and 'moviename' in request.form and 'moviehall' in request.form and 'email' in request.form:
		# Create variables for easy access
		name = request.form['name']
		moviename = request.form['moviename']
		moviehall = request.form['moviehall']
		email = request.form['email']
		cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
		cursor.execute('SELECT * FROM accounts1 WHERE name = %s', (name,))
		account = cursor.fetchone()
		# If account exists show error and validation checks
		if account:
			msg = 'Account already exists!'
		elif not name or not moviename or not moviehall or not email:
			msg = 'Please fill out the form!'
		else:
			# Account doesnt exists and the form data is valid, now insert new account into accounts table
			cursor.execute('INSERT INTO accounts1 VALUES (NULL, %s, %s, %s,%s)', (name, moviename, moviehall,email))
			mysql.connection.commit()
			msg = 'You have successfully Booked your Ticket!'
			email = request.form['email']
			msg = f"Dear {name} your ticket has been succesfully booked for {moviename} at {moviehall}"
			message = Message(sender="sriramsriramsriram19@gmail.com",recipients=[email])
			message.body=msg 
			mail.send(message)
		# success = "Message sent"
	return render_template('x.html', msg=msg)


@app.route('/pythonlogin/profile1/<int:id>')
#this function gives the description of user
def profile1(id):
    if 'loggedin' in session:
        cur=mysql.connection.cursor()
        cur.execute("select * from person where id=%s",(id))
        data=dict(cur.fetchone())
        return data
    else:
        return {'message':"Login First"},401


if __name__=='__main__':
    app.run(debug=True,port=41025)
